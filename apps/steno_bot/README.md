# StenoBot

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `steno_bot` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:steno_bot, "~> 0.1.0"}]
    end
    ```

  2. Ensure `steno_bot` is started before your application:

    ```elixir
    def application do
      [applications: [:steno_bot]]
    end
    ```

