defmodule StenoWeb.Shell do
  use GenServer

  ### Interface ###

  def start(topic) do
    state = %{
      topic: topic,
      chunks: [],
    }
    GenServer.start_link(__MODULE__, state)
  end

  def run(pid, cmd) do
    GenServer.call(pid, {:run, cmd})
  end

  ### Implementation ###

  def handle_call({:run, cmd}, _from, state) do
    proc = Porcelain.spawn_shell(
      cmd, 
      out: {:send, self},
      err: {:send, self},
    )
    state = Map.put(state, :proc, proc)
    {:reply, :ok, state}
  end

  def handle_info({_pid, :data, :out, data}, state) do
    chunks = [ data | state[:chunks] ]
    send_channel(state[:topic], "chunks", %{ "chunks" => Enum.reverse(chunks) })
    {:noreply, %{ state | chunks: chunks }}
  end
  
  def handle_info({_pid, :data, :err, data}, state) do
    chunks = [ data | state[:chunks] ]
    send_channel(state[:topic], "chunks", %{ "chunks" => Enum.reverse(chunks) })
    {:noreply, %{ state | chunks: chunks }}
  end
  
  def handle_info({_pid, :result, result}, state) do
    #{:stop, "done", "done"}
    {:noreply, state}
  end

  def send_channel(topic, tag, msg) do
    StenoWeb.Endpoint.broadcast(topic, tag, msg)
  end
end
