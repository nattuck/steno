defmodule StenoWeb.Sandbox do
  use GenServer
  ##
  ## interface
  ## 
  def start_link(sandbox_id) do
    queue = [{"launch", sandbox_name(sandbox_id)}]
    #queue = []

    GenServer.start_link(__MODULE__, 
      %{
        sb_id: sandbox_id,
        serial: 0,
        output: [],
        queue:  :queue.from_list(queue),
        active: nil,
      },
      name: {:via, :gproc, {:n, :l, {:sandbox, sandbox_id}}}
    )
  end

  def find(sandbox_id) do
    pid = :gproc.where({:n, :l, {:sandbox, sandbox_id}})
    case pid do
      :undefined -> nil
      _ -> pid
    end
  end

  def run(pid, cmd) do
    GenServer.call(pid, {:run, cmd})
  end

  def dump(pid) do
    GenServer.call(pid, :dump)
  end

  def stop(pid) do
    GenServer.call(pid, :stop)
  end

  def cleanup(sandbox_id) do
    script = script_path("stop")
    sbname = sandbox_name(sandbox_id)
    cmd    = ~s(bash "#{script}" "#{sbname}")
    Porcelain.shell(cmd)
  end

  ##
  ## implementation 
  ##
  def init(state) do
    {:ok, state |> spawn_next}
  end

  def handle_call({:run, cmd}, _from, state) do
    state = %{ state | queue: :queue.cons(cmd, state.queue) }
    |> spawn_next
    {:reply, :ok, state}
  end

  def handle_call(:dump, _from, state) do
    {:reply, state, state}
  end

  def handle_call(:stop, _from, state) do
    {:stop, :normal, :ok, state}
  end

  defp spawn_cmd({script, arg}) do
    Porcelain.spawn_shell(
      ~s(bash "#{script_path(script)}" "#{arg}"),
      out: {:send, self()},
      err: {:send, self()})
  end

  defp spawn_next(state) do
    if state.active || :queue.is_empty(state.queue) do
      state
    else
      next = :queue.get(state.queue)
      proc = spawn_cmd(next)
      
      state
      |> Map.put(:queue,  :queue.drop(state.queue))
      |> Map.put(:active, proc)
    end
  end

  defp script_path(name) do
    base = Application.app_dir(:steno_web, "priv")
    "#{base}/scripts/sandbox/#{name}.sh"
  end

  defp sandbox_name(sandbox_id) do
    "steno-sb-#{sandbox_id}"
  end

  ##
  ## callbacks
  ##
  def handle_info({_src, :data, stream, data}, state) do
    serial = state.serial + 1
    IO.inspect {serial, stream, data}
    state = state
    |> Map.put(:output, [ {serial, stream, data} | state.output ])
    |> Map.put(:serial, serial)
    {:noreply, state}
  end 
  
  def handle_info({_src, :result, _result}, state) do
    state = state
    |> Map.put(:active, nil)
    |> spawn_next
    {:noreply, state}
  end
end

