defmodule StenoWeb.Upload do
  def make_key do
    :crypto.strong_rand_bytes(15) |> Base.encode32
  end

  def put!(key, upload) do
    dir = directory(key)
    :ok = File.mkdir_p(dir)
    {:ok, _} = File.copy(upload.path, path(key, upload.filename))
    :ok
  end

  def path(key, name) do
    "#{directory(key)}/#{name}"
  end

  def directory(key) do
    prefix = String.slice(key, 0, 2)
    "#{base_dir()}/#{prefix}/#{key}"
  end

  def base_dir do
    Path.expand("~/.local/steno/uploads")
  end

  def cleanup!(key) do
    dd = directory(key)
    if String.length(key) > 8 && dd =~ "steno/uploads" do
      File.rm_rf!(dd)
    end
  end

  def from_param(params, kk, name) do
    up = Map.get(params, name)
    if up do
      :ok = put!(kk, up)
      up.filename
    else
      nil
    end
  end

  def from_params(params, kk, names) do
    data = %{ "upkey" => kk }
    Enum.reduce names, data, fn(nn, acc) ->
      up = Map.get(params, nn)
      if up do
        :ok = put!(kk, up)
        Map.put(acc, nn, up.filename)
      else
        acc
      end
    end
  end
end
