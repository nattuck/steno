defmodule StenoWeb.Repo.Migrations.CreatePlan do
  use Ecto.Migration

  def change do
    create table(:plans) do
      add :upkey, :string
      add :setup, :string
      add :prep, :string
      add :build, :string
      add :grade, :string

      timestamps()
    end

  end
end
