defmodule StenoWeb.Repo.Migrations.CreateJob do
  use Ecto.Migration

  def change do
    create table(:jobs) do
      add :upkey, :string
      add :plan_id, :integer
      add :file, :string
      add :output, :text
      add :result, :text

      timestamps()
    end

  end
end
