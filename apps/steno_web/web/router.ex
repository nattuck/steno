defmodule StenoWeb.Router do
  use StenoWeb.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", StenoWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/shell", PageController, :shell

    resources "/plans", PlanController
    resources "/jobs", JobController
  end

  # Other scopes may use custom stacks.
  # scope "/steno/v1", StenoWeb do
  #   pipe_through :api
  # end
end
