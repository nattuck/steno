defmodule StenoWeb.Job do
  use StenoWeb.Web, :model

  schema "jobs" do
    field :plan_id, :integer
    field :file, :string
    field :output, :string
    field :result, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:plan_id, :file, :output, :result])
    |> validate_required([:plan_id, :file])
  end
end
