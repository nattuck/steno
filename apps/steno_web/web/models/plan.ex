defmodule StenoWeb.Plan do
  use StenoWeb.Web, :model

  schema "plans" do
    field :setup, :string
    field :prep,  :string
    field :build, :string
    field :grade, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:setup, :prep, :build, :grade])
    |> validate_required([])
  end
end
