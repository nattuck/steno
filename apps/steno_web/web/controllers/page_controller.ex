defmodule StenoWeb.PageController do
  use StenoWeb.Web, :controller

  def index(conn, _params) do
    new_plan = StenoWeb.Plan.changeset(%StenoWeb.Plan{})
    new_job  = StenoWeb.Job.changeset(%StenoWeb.Job{})
    render conn, "index.html", new_plan: new_plan, new_job: new_job
  end

  def shell(conn, _params) do
    render conn, "shell.html"
  end
end
