defmodule StenoWeb.SandboxChannel do
  use StenoWeb.Web, :channel

  def join("sandbox:" <> sandbox_id, payload, socket) do
    if authorized?(payload) do
      {:ok, sandbox} = StenoWeb.Shell.start("sandbox:" <> sandbox_id)
      {:ok, assign(socket, :sandbox, sandbox)}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  def handle_in("run", payload, socket) do
    StenoWeb.Shell.run(socket.assigns[:sandbox], payload)
    {:reply, {:ok, %{}}, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
