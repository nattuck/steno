# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :steno_web,
  ecto_repos: [StenoWeb.Repo]

# Configures the endpoint
config :steno_web, StenoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "WsbL+JQoT98R+d960k55F7Vv40l+eVa+bDVi08qbF9psd0nRJ9w0CpSQps1cZ0N2",
  render_errors: [view: StenoWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: StenoWeb.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
