defmodule StenoWeb.JobTest do
  use StenoWeb.ModelCase

  alias StenoWeb.Job

  @valid_attrs %{file: "some content", plan_id: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Job.changeset(%Job{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Job.changeset(%Job{}, @invalid_attrs)
    refute changeset.valid?
  end
end
