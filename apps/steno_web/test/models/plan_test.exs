defmodule StenoWeb.PlanTest do
  use StenoWeb.ModelCase

  alias StenoWeb.Plan

  @valid_attrs %{build: "some content", grading: "some content", output: "some content", prep: "some content", result: "some content", setup: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Plan.changeset(%Plan{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Plan.changeset(%Plan{}, @invalid_attrs)
    refute changeset.valid?
  end
end
