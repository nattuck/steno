defmodule StenoWeb.PlanControllerTest do
  use StenoWeb.ConnCase

  alias StenoWeb.Plan
  @valid_attrs %{build: "some content", grading: "some content", output: "some content", prep: "some content", result: "some content", setup: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, plan_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing plans"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, plan_path(conn, :new)
    assert html_response(conn, 200) =~ "New plan"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, plan_path(conn, :create), plan: @valid_attrs
    assert redirected_to(conn) == plan_path(conn, :index)
    assert Repo.get_by(Plan, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, plan_path(conn, :create), plan: @invalid_attrs
    assert html_response(conn, 200) =~ "New plan"
  end

  test "shows chosen resource", %{conn: conn} do
    plan = Repo.insert! %Plan{}
    conn = get conn, plan_path(conn, :show, plan)
    assert html_response(conn, 200) =~ "Show plan"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, plan_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    plan = Repo.insert! %Plan{}
    conn = get conn, plan_path(conn, :edit, plan)
    assert html_response(conn, 200) =~ "Edit plan"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    plan = Repo.insert! %Plan{}
    conn = put conn, plan_path(conn, :update, plan), plan: @valid_attrs
    assert redirected_to(conn) == plan_path(conn, :show, plan)
    assert Repo.get_by(Plan, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    plan = Repo.insert! %Plan{}
    conn = put conn, plan_path(conn, :update, plan), plan: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit plan"
  end

  test "deletes chosen resource", %{conn: conn} do
    plan = Repo.insert! %Plan{}
    conn = delete conn, plan_path(conn, :delete, plan)
    assert redirected_to(conn) == plan_path(conn, :index)
    refute Repo.get(Plan, plan.id)
  end
end
